package com.gradimut.paycalculator.Utils

interface IPreferenceHelper {
    fun setFirstTime(firstTime: String)
    fun getFirstTime()  : String
    fun clearPrefs()
}