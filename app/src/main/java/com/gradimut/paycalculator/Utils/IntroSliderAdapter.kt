package com.gradimut.paycalculator.Utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gradimut.paycalculator.R
import kotlinx.android.synthetic.main.slider_page.view.*

class IntroSliderAdapter(private val introSlideList: List<IntroSlider>):
    RecyclerView.Adapter<IntroSliderAdapter.IntroSlideViewHolder>() {
    inner class IntroSlideViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val textTitle = view.findViewById<TextView>(R.id.slide_heading)
        private val textDescription = view.findViewById<TextView>(R.id.slide_desc)
        private val imageIcon = view.findViewById<ImageView>(R.id.slide_image)

        fun bind(introSlider: IntroSlider) {
            textTitle.text = introSlider.title
            textDescription.text = introSlider.description
            imageIcon.setBackgroundResource(introSlider.icon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroSlideViewHolder {
        return IntroSlideViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.slider_page,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return introSlideList.size
    }

    override fun onBindViewHolder(holder: IntroSlideViewHolder, position: Int) {
        holder.bind(introSlideList[position])
    }
}