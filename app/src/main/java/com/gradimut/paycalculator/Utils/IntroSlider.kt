package com.gradimut.paycalculator.Utils

data class IntroSlider(
    val title: String,
    val description: String,
    val icon: Int
)