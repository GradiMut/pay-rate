package com.gradimut.paycalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import com.gradimut.paycalculator.Utils.IntroSlider
import com.gradimut.paycalculator.Utils.IntroSliderAdapter
import androidx.viewpager2.widget.ViewPager2
import com.gradimut.paycalculator.Utils.IPreferenceHelper
import com.gradimut.paycalculator.Utils.PreferenceManager
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoarding : AppCompatActivity() {
    private val preferenceHelper: IPreferenceHelper by lazy { PreferenceManager(applicationContext) }


    private val introSliderAdapter = IntroSliderAdapter(
        listOf(
            IntroSlider(
                "TIME IS MONEY",
                "Monetize your working hours by rating it according to the number of hours you work per day ",
                R.drawable.ic_time_is_money
            ),
            IntroSlider(
                "RATE YOUR TIME",
                "Get real estimation of what you can make, based on your working hours",
                R.drawable.ic_rate_your_time
            ),
            IntroSlider(
                "VISUALIZE",
                "Get a projection of your yearly, monthly, weekly and daily income in a easily way",
                R.drawable.ic_know_what_you_value
            )
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        slideViewPager.adapter = introSliderAdapter
        setUpIndicators()
        setUpCurrentIndicator(0)


        slideViewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setUpCurrentIndicator(position)



                if (position >= 1) {
                    prevBtn.visibility = View.VISIBLE
                } else {
                    prevBtn.visibility = View.GONE
                }

                if (position == 2) {
                    nextBtn.setText(R.string.finish)

                } else {
                    nextBtn.setText(R.string.next)
                }

            }
        })


        nextBtn.setOnClickListener{
            if (slideViewPager.currentItem + 1 <  introSliderAdapter.itemCount) {
                slideViewPager.currentItem += 1

            } else {
                Intent(applicationContext, MainActivity::class.java).also {
                    preferenceHelper.setFirstTime("seen")
                    startActivity(it)
                    finish()
                }
            }
        }

        prevBtn.setOnClickListener {
            if (slideViewPager.currentItem + 1 <= introSliderAdapter.itemCount) {
                slideViewPager.currentItem -= 1
            }
        }



//        if (slideViewPager.currentItem + 1 == 2) {
//            nextBtn.setText(R.string.finish)
//        } else {
//            nextBtn.setText(R.string.next)
//        }

        if (slideViewPager.currentItem + 1 > 1) {
            prevBtn.visibility = View.VISIBLE
            prevBtn.setText(R.string.back)
        }
    }

    private fun setUpIndicators() {
        val indicators = arrayOfNulls<ImageView>(introSliderAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8,0,8,0)

        for(i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )

                this?.layoutParams = layoutParams
            }

            dotsLayout.addView(indicators[i])
        }
    }

    private fun setUpCurrentIndicator(index: Int) {
        val childCount = dotsLayout.childCount

        for (i in 0 until childCount) {
            val imageView = dotsLayout[i] as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }
}