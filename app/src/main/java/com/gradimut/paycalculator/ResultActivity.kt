package com.gradimut.paycalculator

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import java.text.NumberFormat
import java.util.*

class ResultActivity : AppCompatActivity() {
    private lateinit var monthlyTextView: TextView
    private lateinit var yearlyTextView: TextView
    private lateinit var weeklyTextView: TextView
    private lateinit var dailyTextView: TextView
    private lateinit var hourlyTextView: TextView
    private lateinit var linearLayoutView: LinearLayout
    private lateinit var boxConstraint: ConstraintLayout
    private lateinit var mth_pay: TextView
    private lateinit var recalculateBtn: Button

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)


        monthlyTextView = findViewById(R.id.budget_txt)
        yearlyTextView = findViewById(R.id.annually_budget)
        weeklyTextView = findViewById(R.id.weekly_budget)
        dailyTextView = findViewById(R.id.daily_budget)
        hourlyTextView = findViewById(R.id.hourly_budget)
        linearLayoutView = findViewById(R.id.linearLayout2)
        boxConstraint = findViewById(R.id.boxConstraint)
        mth_pay = findViewById(R.id.mth_pay)
        recalculateBtn = findViewById(R.id.recalculateBtn)



//         Getting value from @MainActivity
        val bundle = intent.extras
        val yearly = bundle?.getDouble("yearly")
        val monthly = bundle?.getDouble("monthly")
        val weekly = bundle?.getDouble("weekly")
        val daily = bundle?.getDouble("daily")
        val hourly = bundle?.getDouble("hourly")


//         Displaying value
        monthlyTextView.text =
            "$ " + "\t" + NumberFormat.getNumberInstance(Locale.US).format(monthly).toString()
        yearlyTextView.text = NumberFormat.getNumberInstance(Locale.US).format(yearly).toString()
        weeklyTextView.text = NumberFormat.getNumberInstance(Locale.US).format(weekly).toString()
        dailyTextView.text = NumberFormat.getNumberInstance(Locale.US).format(daily).toString()
        hourlyTextView.text = NumberFormat.getNumberInstance(Locale.US).format(hourly).toString()

        animation()

        recalculateBtn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }


    }

    private fun animation () {
        val layoutAnimation = AnimationUtils.loadAnimation(this, R.anim.boarding)
        val titleAnimation = AnimationUtils.loadAnimation(this, R.anim.layout_animation)
        val btnAnimation = AnimationUtils.loadAnimation(this, R.anim.button_animation)

        // Start animation
        mth_pay.startAnimation(titleAnimation)
        linearLayoutView.startAnimation(layoutAnimation)
        boxConstraint.startAnimation(btnAnimation)


    }



}