package com.gradimut.paycalculator

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    // variable
    private lateinit var selectDays: Spinner
    private lateinit var mCalculate: Button
    private lateinit var hourlyRate: EditText
    private lateinit var dailyHour: EditText
    private lateinit var mDaysWorkedPerWeek: Number
    private lateinit var mLayout: LinearLayout
    private lateinit var layoutTitle: TextView
    private lateinit var hrTxt: TextView
    private lateinit var drTxt: TextView
    private lateinit var dpwTxt: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mLayout = findViewById(R.id.top_layout)
        layoutTitle = findViewById(R.id.textView3)
        hrTxt = findViewById(R.id.hr_txt)
        drTxt = findViewById(R.id.dr_txt)
        dpwTxt = findViewById(R.id.dpw_txt)
        hourlyRate = findViewById(R.id.hourly_rate)
        dailyHour = findViewById(R.id.daily_hours)
        selectDays = findViewById(R.id.select_days)
        mCalculate = findViewById(R.id.calculate)

        //Start animation
        animation()

        // Get String array to from the resource and attach it to the spinner when it pop out
        val daysPerWeek = getResources().getStringArray(R.array.spinner_attach)
        // Customizing our spinner by selecting the layout we want to use
        val daysWorkedPerWeek = ArrayAdapter<String>(
            this,
            R.layout.days_select,
            R.id.tvSelect,
            daysPerWeek
        ).also { arrayAdapter ->
            arrayAdapter.setDropDownViewResource(R.layout.drop_down_ui)

        }

        // Attach the change to the adapter
        selectDays.adapter = daysWorkedPerWeek

        // onClick lister on the spinner
        selectDays.setOnItemSelectedListener(object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent:AdapterView<*>, view:View, position:Int, id:Long) {
//                val days = (selectDays.getSelectedItem()).toString()

                when (position) {
                    0 -> mDaysWorkedPerWeek = 0
                    1 -> mDaysWorkedPerWeek = 1
                    2 -> mDaysWorkedPerWeek = 2
                    3 -> mDaysWorkedPerWeek = 3
                    4 -> mDaysWorkedPerWeek = 4
                    5 -> mDaysWorkedPerWeek = 5
                    6 -> mDaysWorkedPerWeek = 6
                    7 -> mDaysWorkedPerWeek = 7

                    else -> {
                        Log.d("Selection", "Select days worked per week")
                    }
                }
            }
            override fun onNothingSelected(parent:AdapterView<*>) {
                // When nothing is selected.
            }
        })


        // perform calculation and launch new activity
        mCalculate.setOnClickListener{
            // get text from TextView
            val payPerHour = hourlyRate.text
            val dayPerHour = dailyHour.text
            val daysNumber =  mDaysWorkedPerWeek

            val weekPerMonth = 4
            val numberOfMonth = 12

            // perform validation
            if (payPerHour.isEmpty()) {
                Toast.makeText(applicationContext, "Hourly rate field is empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (dayPerHour.isEmpty()) {
                Toast.makeText(applicationContext, "Daily hour field is empty", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (daysNumber == 0) {
                Log.d("Checking 2", "Okay")
                Toast.makeText(applicationContext, "Please select a valid option", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            // perform calculation
            val perDay = payPerHour.toString().toDouble() * dayPerHour.toString().toDouble()
            val perWeek = perDay * daysNumber.toString().toDouble()
            val perMonth = perWeek * weekPerMonth
            val perYear = perMonth * numberOfMonth


            // launch new activity
            val intent = Intent(this, ResultActivity::class.java)
            intent.putExtra("yearly", perYear)
            intent.putExtra("monthly", perMonth)
            intent.putExtra("daily", perDay)
            intent.putExtra("weekly", perWeek)
            intent.putExtra("hourly", payPerHour.toString().toDouble())
            startActivity(intent)
        }
    }

    private fun animation() {
        // Get resource animation
        val layoutAnimation = AnimationUtils.loadAnimation(this, R.anim.boarding)
        val titleAnimation = AnimationUtils.loadAnimation(this, R.anim.layout_animation)
        val textAnimation = AnimationUtils.loadAnimation(this, R.anim.text_animation)
        val boxAnimation = AnimationUtils.loadAnimation(this, R.anim.box_animation)
        val btnAnimation = AnimationUtils.loadAnimation(this, R.anim.button_animation)

        // Start animation
        mLayout.startAnimation(layoutAnimation)
        layoutTitle.startAnimation(titleAnimation)
        hrTxt.startAnimation(textAnimation)
        drTxt.startAnimation(textAnimation)
        dpwTxt.startAnimation(textAnimation)
        hourlyRate.startAnimation(boxAnimation)
        dailyHour.startAnimation(boxAnimation)
        selectDays.startAnimation(boxAnimation)
        mCalculate.startAnimation(btnAnimation)

    }

    override fun onResume() {
        super.onResume()
        Log.i("Resume Method", "onResume called")
    }

    override fun onStart() {
        super.onStart()
        animation()
        Log.i("Start Method", "onStart called")
    }

    override fun onStop() {
        super.onStop()
        Log.i("Stop Method", "onStop called")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("Destroy Method", "onDestroy called")
    }


}