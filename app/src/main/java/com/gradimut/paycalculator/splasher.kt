package com.gradimut.paycalculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.gradimut.paycalculator.Utils.IPreferenceHelper
import com.gradimut.paycalculator.Utils.PreferenceManager


class splasher : AppCompatActivity() {

    private val preferenceHelper: IPreferenceHelper by lazy { PreferenceManager(applicationContext) }

    lateinit var handler: Handler
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splasher)

        // get transition animation
        val splashImageAnimation = AnimationUtils.loadAnimation(this, R.anim.splash)

        // get image view
        val splashImage = findViewById<ImageView>(R.id.splashImage)

        // Launch animation
        splashImage.startAnimation(splashImageAnimation)

        handler = Handler()

        handler.postDelayed({
            val getPref = preferenceHelper.getFirstTime()
            if (getPref == "seen") {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this, OnBoarding::class.java)
                startActivity(intent)
                finish()
            }
        }, 2000)

    }
}